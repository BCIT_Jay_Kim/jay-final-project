#!/bin/bash

#===============================================================================
#          FILE: hostapd_setup.sh
#        AUTHOR: Jay Kim
#  ORGANIZATION: BCIT
#       CREATED: 06/11/2017
#===============================================================================

#install hostapd package
yum -y install hostapd

#edit hostapd file
echo '
#
# This will give you a minimal, insecure wireless network.
#
# DO NOT BE SATISFIED WITH THAT!!!
#
# A complete, well commented example configuration file is
# available here:
#
#	/usr/share/doc/hostapd/hostapd.conf
#
# For more information, look here:
#
#	http://wireless.kernel.org/en/users/Documentation/hostapd
#

ctrl_interface=/var/run/hostapd
ctrl_interface_group=wheel

# Some usable default settings...
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0

# Uncomment these for base WPA & WPA2 support with a pre-shared key
wpa=2
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP

# DO NOT FORGET TO SET A WPA PASSPHRASE!!
wpa_passphrase=p@ssw0rd

# Most modern wireless drivers in the kernel need driver=nl80211
driver=nl80211

# Customize these for your local configuration...
interface=wlp0s6u1
hw_mode=g
channel=1
ssid=NASP19_S10
' > /etc/hostapd/hostapd.conf

# restarting network services
systemctl restart network.service

# starting and enabling hostapd service
systemctl start hostapd
systemctl enable hostapd

# Confirmation of succesfull configuration
echo "The hostapd configuration is done."
