#!/bin/bash

#===============================================================================
#          FILE: network_setup.sh
#        AUTHOR: Jay Kim
#  ORGANIZATION: BCIT
#       CREATED: 06/11/2017
#===============================================================================

#disabling Network Manager
echo "Turning off and disabling Network Manager"
systemctl stop NetworkManager.service
systemctl disable NetworkManager.service

#disabling firewall
echo "Turning off and disabling Firewall Daemon"
systemctl stop firewalld.service
systemctl disable firewalld.service

# restarting network services
systemctl restart network.service

#call scripts to configure interfaces : net_if_setup.sh and wifi_if_setup.sh

#Configure OSPF
yum -y install quagga

#Change the ownership of the /etc/quagga/
chmod 775 /etc/quagga
cp /usr/share/doc/quagga-0.99.22.4/zebra.conf.sample /etc/quagga/zebra.conf

#Start and enable the zebra routing management daemon
systemctl start zebra
systemctl enable zebra

#Copy zebra configuration
echo '
!
! Zebra configuration saved from vty
!   2017/06/10 10:10:10
!
hostname s10rtr.as.learn
log file /var/log/quagga/quagga.log
!
interface eth0
 description External
 ip address 10.16.255.10/24
 ipv6 nd suppress-ra
!
interface eth1
 description Internal
 ip address 10.16.10.126/25
 ipv6 nd suppress-ra
!
interface lo
!
ip forwarding
!
!
line vty
!
' > /etc/quagga/zebra.conf

#Start and enable ospfd Daemon
systemctl start ospfd
systemctl enable ospfd
chown quagga ospfd.conf

#Copy ospfd configuration
echo '
!
! Zebra configuration saved from vty
!   2017/05/31 16:12:32
!
hostname ospfd
password zebra
log stdout
!
!
!
interface eth0
!
interface eth1
!
interface lo
!
router ospf
 ospf router-id 10.16.255.10
 network 10.16.10.0/25 area 0.0.0.0
 network 10.16.10.128/25 area 0.0.0.0
 network 10.16.255.0/24 area 0.0.0.0
 !
 line vty
 !
 ' > /etc/quagga/ospfd.conf

 # Confirmation of succesfull configuration
 echo "The OSPF configuration is done."
