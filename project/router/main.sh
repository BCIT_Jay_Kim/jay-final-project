#!/bin/bash

#===============================================================================
#          FILE: main.sh
#        AUTHOR: Jay Kim
#  ORGANIZATION: BCIT
#       CREATED: 06/11/2017
#===============================================================================

source network_setup.sh

source base_configuration.sh

source selinux_setup.sh

source nsd_setup.sh

source unbound_setup.sh

source dhcpd_setup.sh

source hostapd_setup.sh

source postfix_setup.sh

source dovecot_setup.sh

source iptables_setup.sh

# Confirmation of succesfull configuration
echo "All configuration are done."
