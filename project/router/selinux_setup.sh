#!/bin/bash

#===============================================================================
#          FILE: selinux_setup.sh
#        AUTHOR: Jay Kim
#  ORGANIZATION: BCIT
#       CREATED: 06/11/2017
#===============================================================================

# disabling selinux
echo "Disabling selinux"
setenforce 0
sed -r -i 's/SELINUX=(enforcing|disabled)/SELINUX=permissive/' /etc/selinux/config

# Confirmation of succesfull configuration
echo "The selinux configuration is done."
