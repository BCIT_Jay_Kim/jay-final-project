#!/bin/bash

#===============================================================================
#          FILE: dhcpd_setup.sh
#        AUTHOR: Jay Kim
#  ORGANIZATION: BCIT
#       CREATED: 06/11/2017
#===============================================================================

# Install dhcp service
yum -y install dhcp

#copy file
cp /usr/lib/systemd/system/dhcpd.service /etc/systemd/system/dhcpd.service

#add the interface to listen
echo '
[Unit]
Description=DHCPv4 Server Daemon
Documentation=man:dhcpd(8) man:dhcpd.conf(5)
Wants=network-online.target
After=network-online.target
After=time-sync.target

[Service]
Type=notify
ExecStart=/usr/sbin/dhcpd -f -cf /etc/dhcp/dhcpd.conf -user dhcpd -group dhcpd --no-pid eth1 wlp0s6u1

[Install]
WantedBy=multi-user.target
' > etc/systemd/system/dhcp.service

#add dhcp configuration
echo '
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp*/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#

subnet 10.16.10.0 netmask 255.255.255.128{
option routers 10.16.10.126;
option domain-name "s10.as.learn";
option domain-name-servers 10.16.10.126;
range 10.16.10.100 10.16.10.125;}

subnet 10.16.10.128 netmask 255.255.255.128{
option routers 10.16.10.254;
range 10.16.10.150 10.16.10.180;}

host mailserver{
hardware ethernet 08:00:27:61:89:cd;
fixed-address 10.16.10.1;}
' > etc/dhcp/dhcpd.conf

# start dhcp services
systemctl enable dhcpd
systemctl start dhcpd

# Confirmation of succesfull configuration
echo "The dhcpd configuration is done."
