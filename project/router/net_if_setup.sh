#!/bin/bash

#===============================================================================
#          FILE: net_if_setup.sh
#        AUTHOR: Jay Kim
#  ORGANIZATION: BCIT
#       CREATED: 06/11/2017
#===============================================================================

#configuration on Interface eth0
echo '
VLAN=yes
VLAN_ID=2016
BOOTPROTO=none
DEFROUTE=yes
PEERDNS=yes
PEERROUTES=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_PEERDNS=yes
IPV6_PEERROUTES=yes
IPV6_FAILURE_FATAL=no
IPV6_ADDR_GEN_MODE=stable-privacy
NAME=eth0
UUID=9481ea70-49e5-459c-9840-15d4022c48a8
DEVICE=eth0
ONBOOT=yes
IPADDR=10.16.255.10
PREFIX=24
GATEWAY=10.16.255.254
DNS1=10.16.255.10
DNS2=142.232.221.253
' > /etc/sysconfig/network-scripts/ifcfg-eth0

#configuration on Interface eth1
echo '
TYPE=Ethernet
BOOTPROTO=none
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
IPV6_ADDR_GEN_MODE=stable-privacy
NAME=eth1
UUID=86723594-d160-4920-b793-0eca35dd79f3
DEVICE=eth1
ONBOOT=yes
IPADDR=10.16.10.126
PREFIX=25
IPV6_PEERDNS=yes
IPV6_PEERROUTES=yes
IPV6_PRIVACY=no
' > /etc/sysconfig/network-scripts/ifcfg-eth1

# restarting network services
systemctl restart network.service

# Confirmation of succesfull configuration
echo "The net_if configuration is done."
