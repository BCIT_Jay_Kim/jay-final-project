#!/bin/bash

#===============================================================================
#          FILE: iptables_setup.sh
#        AUTHOR: Jay Kim
#  ORGANIZATION: BCIT
#       CREATED: 06/11/2017
#===============================================================================

#Download iptables service
yum -y install iptables-services

systemctl enable iptables
systemctl start iptables

#Flush the OLD configuration of iptables
iptables -F

#DEFAULT POLICY
iptables --policy INPUT DROP
iptables --policy FORWARD DROP


#ACCEPT EVERYTHING ON LOOPBACK
iptables -A INPUT -i lo -j ACCEPT

#ALLOW ESTABLISHED AND RELATED CONNECTIONS
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

#ALLOW OSPF
iptables -A INPUT -i eth0 -d 224.0.0.5 -p ospf -j ACCEPT
iptables -A INPUT -i eth0 -d 224.0.0.6 -p ospf -j ACCEPT

#ALLOW SSH
iptables -A INPUT -i eth0 -p tcp --dport ssh -j ACCEPT

#ALLOW ICMP
iptables -A INPUT -p icmp -j ACCEPT

#ALLOW DNS
iptables -A INPUT -p tcp --dport 53 -j ACCEPT
iptables -A INPUT -p udp --dport 53 -j ACCEPT

#iptables -A INPUT -i eth1 -p tcp --dport 53 -j ACCEPT
#iptables -A INPUT -i eth1 -p udp --dport 53 -j ACCEPT


#ALLOW INTO STUDENT
iptables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A FORWARD -i eth1 -o eth0 -j ACCEPT
#iptables -A FORWARD -i eth0 -o eth1 -j ACCEPT
iptables -A FORWARD -i eth0 -d 10.16.10.0/25 -j ACCEPT

#LOG GENERATE
iptables -A INPUT -j LOG

service iptables save
systemctl restart iptables

# Confirmation of succesfull configuration
echo "The iptables configuration is done."
