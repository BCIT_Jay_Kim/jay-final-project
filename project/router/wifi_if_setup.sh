#!/bin/bash -

#===============================================================================
#          FILE: wifi_if_setup.sh
#        AUTHOR: Jay Kim
#  ORGANIZATION: BCIT
#       CREATED: 06/11/2017
#===============================================================================


# edit wireless interface configuration file ifcfg-wlp0s6u1

echo
'
DEVICE="wlp0s6u1"
BOOTPROTO="none"
TYPE="Wireless"
ONBOOT=yes
NM_CONTROLLED=no
IPADDR=10.16.10.254
PREFIX=25
ESSID="NASP19_S10"
CHANNEL="2"
MODE="Master"
RATE="Auto"
' > /etc/sysconfig/network-scripts/ifcfg-wlp0s6u1

# restarting network services
systemctl restart network.service

# Confirmation of succesfull configuration
echo "The Wifi-if configuration is done."
