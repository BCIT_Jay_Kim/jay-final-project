#!/bin/bash

#===============================================================================
#          FILE: dovecot_setup.sh
#        AUTHOR: Jay Kim
#  ORGANIZATION: BCIT
#       CREATED: 06/11/2017
#===============================================================================

# Install dovecot package
yum -y install dovecot

#enable dovecot service at boot
systemctl enable dovecot

#Configure dovecot protocols
echo '
protocols = "imap lmtp"
dict {
}
!include conf.d/*.conf
!include_try local.conf
' > /etc/dovecot/dovecot.conf

# Specify mail location
echo '
mail_location = maildir:~/Maildir
namespace inbox {
  inbox = yes
}
first_valid_uid = 1000
mbox_write_locks = fcntl
' > /etc/dovecot/conf.d/10-mail.conf

#Allow plaintext authentication and set the authentication mechanism
echo '
disable_plaintext_auth = no
auth_mechanisms = plain
!include auth-system.conf.ext
' > /etc/dovecot/conf.d/10-auth.conf

#Restart dovecot service
systemctl restart dovecot

# Confirmation of succesfull configuration
echo "The dovecot configuration is done."
