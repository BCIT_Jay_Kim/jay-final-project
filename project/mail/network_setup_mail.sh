#!/bin/bash

#===============================================================================
#          FILE: network_setup_mail.sh
#        AUTHOR: Jay Kim
#  ORGANIZATION: BCIT
#       CREATED: 06/11/2017
#===============================================================================

#disabling Network Manager
echo "Turning off and disabling Network Manager"
systemctl stop NetworkManager.service
systemctl disable NetworkManager.service

#disabling firewall
echo "Turning off and disabling Firewall Daemon"
systemctl stop firewalld.service
systemctl disable firewalld.service

#changes hostname to mail.s23.as.learn
hostnamectl set-hostname mail.s10.as.learn

#call scripts to configure interface : net_if_setup_mail.sh
source net_if_setup_mail.sh
