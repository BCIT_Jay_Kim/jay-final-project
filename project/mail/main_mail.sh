#!/bin/bash

#===============================================================================
#          FILE: main_mail.sh
#        AUTHOR: Jay Kim
#  ORGANIZATION: BCIT
#       CREATED: 06/11/2017
#===============================================================================


#calls script network_setup_mail.sh: configure networking on the system
source network_setup_mail.sh

#calls script base_configuration_mail.sh: configure basic system
source base_configuration_mail.sh

#calls script postfix_setup.sh: configure mail transfer agent
source postfix_setup.sh

#calls script dovecot_setup.sh: configure mail delivery agent
source dovecot_setup.sh
