#!/bin/bash

#===============================================================================
#          FILE: net_if_setup_mail.sh
#        AUTHOR: Jay Kim
#  ORGANIZATION: BCIT
#       CREATED: 06/11/2017
#===============================================================================

#configuration on Interface eth0
echo '
TYPE=Ethernet
BOOTPROTO=dhcp
DEFROUTE=yes
IPV4_FAILURE_FATAL=yes
NAME=eth0
DEVICE=eth0
ONBOOT=yes
PEERDNS=yes
PEERROUTES=yes
' > /etc/sysconfig/network-scripts/ifcfg-eth0

# restarting network services
systemctl restart network.service
