#!/bin/bash - 
#===============================================================================
#
#          FILE: whereis_check.sh
# 
#         USAGE: ./whereis_check.sh 
# 
#   DESCRIPTION: Outputs the location of a command binary and returns an exit code
#          of zero if it is avaible or 127 if it can't be found
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/23/2015 12:26
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

#Variable declarations
declare -r SCRIPTVERSION="1.0"
declare Command_name  
declare whereis_output
declare locations
declare NUMARGS
#todo place variable declarations here

#Solicit command that should be searched and store it in the varialbe command
#todo: use read -p statement to get input from user and store it in the variable: cmd
#read -p "Please enter the name of the package you would like to see if is installed: " Command_name
#Store the output of the whereis invocation that searches for the users inputed 
# command in the variable "whereis_output"
#todo variable assigment to output of command - follows form variable=$( command )
NUMARGS=$#
for ((i=1 ; i <= NUMARGS ; i++))
do
    # Parameters `$1` to `$999...` contains individual positional parameters. 
    #printf "%s\n" "\$$i: $1"
   
	Command_name=$1
	echo $Command_name
	whereis_output=$( whereis $Command_name 2> /dev/null )
#echo $whereis_output
#Using the variable whereis_output process the output to drop the command name
#store the output in locations
	locations=$( (echo $whereis_output | cut -d ":" -f 2-) )
#echo $locations

	if [[ "$locations" == "" ]]; then
  #the locations are empty i.e. command not found, tell the user 
  #and set the exit code
  		 echo "$Command_name isn't found"
  	 else

   		echo "$Command_name is found"
   		echo "the location of $Command_name is: $locations"
  
 
  #the locations contains the path to the command tell the user
  #and set the exit code
 
	fi
   shift #Shift moves the postional parameters down one and discards the parameter in position 1
done
#exit $?
  
