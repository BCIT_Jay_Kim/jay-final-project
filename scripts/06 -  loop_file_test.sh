#!/bin/bash -
#===============================================================================
#
#          FILE: basic_file_test.sh
#
#         USAGE: ./basic_file_test.sh
#
#   DESCRIPTION: Outputs the location of a command binary and returns an exit code
#                of zero if it is avaible or 127 if it can't be found
#
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/23/2015 12:26
#      REVISION:  ---
#===============================================================================
#leo
set -o nounset                              # Treat unset variables as an error

declare file_name
declare full_file_name
declare RED='\033[0;31m'
declare NC='\033[0m' #no colour
declare BL='\033[0;34m'
function file_test() {
file_name=$1
#echo $file_name
full_file_name=$( ls $file_name 2> /dev/null )
#echo $full_file_name

if [[ $full_file_name == $file_name ]] ; then
  echo "the filename : $file_name exitsts"
  return 0
else
  echo -e "the filename ${BL} $file_name  ${RED} can not be found"${NC}
  return 1
fi
  exit 0
}
  for ((i=1; i <= $#; i++)); do
    file_test ${!i}
  done
