sed -n '/^ *option/p' dhcp.conf

sed '/^$/d' dhcp.conf

sed '1 i/#NASP10 sed DHCP Configuration' dhcp.conf

sed -e 's/ #.*//' dhcp.conf 

sed -e "s/^\s *option[[:space:]]\+routers.*\;/option routers 10.10.10.1;/g" dhcp.conf

sed -n "s/[[:space:]]range[[:space:]]\+[0-9]*.[0-9]*.[0-9]*.\([0-9]*\)[[:space:]]\+[0-9]*.[0-9]*.[0-9]*.\([0-9]*\)/range \1\-\2/p" dhcp.conf

sed -n "s/[[:space:]]range[[:space:]]\+\([0-9]*.[0-9]*.[0-9]*\).[0-9].*/range \1.200 \1.240/p" dhcp.conf
