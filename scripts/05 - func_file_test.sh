#!/bin/bash - 
#===============================================================================
#
#          FILE: simple_functions.sh
# 

#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Jay Kim
#  ORGANIZATION: BCIT
#       CREATED: 04/20/2017 
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

#Function Declarions

function func1() {
   
if test -e $1 ; then

	echo "The filename: $1 exists" 
	
   else 

   	echo "The filename: $1 can't be found" 
     	
fi

	return 0

}

#main script 
#receives a file name and stores it in $file_name

read -p "please input file name you want to check: " file_name
func1 $file_name

#end












