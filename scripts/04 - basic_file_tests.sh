#!/bin/bash - 
#===============================================================================
#
#          FILE: basic_file_tests.sh
# 
#   DESCRIPTION: test whether file exists or not
# 
#        AUTHOR: Jay Kim
#    ORGANIZATION: BCIT
#       CREATED: 04/20/2017 
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

declare file_name

read -p "please input file name you want to check: " file_name

# test command form

if test -e $file_name  ; then

	echo "The filename: $file_name exists" 
	exit 0

   else 

   	echo "The filename: $file_name cann't be found" 
     	exit 1

fi


